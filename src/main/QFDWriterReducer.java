import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.mapreduce.Reducer;
import java.io.ObjectOutputStream;
import java.io.IOException;
import java.util.HashSet;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.fs.FileSystem;
import java.io.ObjectOutputStream;


public class QFDWriterReducer extends Reducer<WTRKey, RequestReplyMatch, NullWritable, NullWritable> {

    @Override
    public void reduce(WTRKey key, Iterable<RequestReplyMatch> values,
                       Context ctxt) throws IOException, InterruptedException {

        // The input will be a WTR key and a set of matches.

        // You will want to open the file named
        // "qfds/key.getName()/key.getName()_key.getHashBytes()"
        // using the FileSystem interface for Hadoop.

        // EG, if the key's name is srcIP and the hash is 2BBB,
        // the filename should be qfds/srcIP/srcIP_2BBB

        // Some useful functionality:

        // FileSystem.get(ctxt.getConfiguration())
        // gets the interface to the filesysstem
        // new Path(filename) gives a path specification
        // hdfs.create(path, true) will create an
        // output stream pointing to that file

        //Filesystem.get(Configuration)
        //"qfds/key.getName()/key.getName()_key.getHashBytes()"
        //hdfs.create(path, true)
        String name = key.getName();
        String hash = key.getHashBytes();
        Path path = new Path(String.format("qfds/%s/%s_%s", name, name, hash));
        HashSet<RequestReplyMatch> set = new HashSet<RequestReplyMatch>();

        for (RequestReplyMatch match : values) {
            set.add(new RequestReplyMatch(match));
        }

        FileSystem filesys = FileSystem.get(ctxt.getConfiguration());
        ObjectOutputStream objectStream = new ObjectOutputStream(filesys.create(path, true));
        QueryFocusedDataSet qfds = new QueryFocusedDataSet(key.getName(), key.getHashBytes(), set);
        objectStream.writeObject(qfds);
        objectStream.close();
    }
}
