import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import java.util.*;
import java.util.HashSet;

import java.io.IOException;

public class QFDMatcherReducer extends Reducer<IntWritable, WebTrafficRecord, RequestReplyMatch, NullWritable> {

    @Override
    public void reduce(IntWritable key, Iterable<WebTrafficRecord> values,
                       Context ctxt) throws IOException, InterruptedException {

        // The input is a set of WebTrafficRecords for each key,
        // the output should be the WebTrafficRecords for
        // all cases where the request and reply are matched
        // as having the same
        // Source IP/Source Port/Destination IP/Destination Port
        // and have occured within a 10 second window on the timestamp.

        // One thing to really remember, the Iterable element passed
        // from hadoop are designed as READ ONCE data, you will
        // probably want to copy that to some other data structure if
        // you want to iterate mutliple times over the data.

        // ctxt.write should be RequestReplyMatch and a NullWriteable
        HashSet<WebTrafficRecord> requestset = new HashSet<WebTrafficRecord>();
        HashSet<WebTrafficRecord> replyset = new HashSet<WebTrafficRecord>();
        HashSet<RequestReplyMatch> reqrepset = new HashSet<RequestReplyMatch>();
        
        for (WebTrafficRecord value : values) {
            WebTrafficRecord curr = new WebTrafficRecord(value);
            if (curr.getCookie() == null) {
                replyset.add(curr);
            } else {
                requestset.add(curr);
            }
        }


        for (WebTrafficRecord value : requestset) {
            long timestamp = value.getTimestamp();
            for (WebTrafficRecord value2 : replyset) {
                    long timestamp2 = value2.getTimestamp();
                    if (Math.abs(timestamp - timestamp2) <= 10) {
                        RequestReplyMatch reqrep = new RequestReplyMatch(value, value2);
                        reqrepset.add(reqrep);
                    }
            }
        }
        
        for (RequestReplyMatch reqrep : reqrepset) {
            ctxt.write(reqrep, NullWritable.get());
        }



    }
}
