import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import java.io.ObjectInputStream;
import java.io.IOException;
import java.security.MessageDigest;
import org.apache.hadoop.fs.FSDataInputStream;
import java.security.NoSuchAlgorithmException;
import java.nio.charset.StandardCharsets;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.FSDataInputStream;
import java.util.*;
import javax.xml.bind.DatatypeConverter;






public class TotalFailMapper extends Mapper<LongWritable, Text, WTRKey,
                                            RequestReplyMatch> {

    private MessageDigest messageDigest;
    @Override
    public void setup(Context ctx) throws IOException, InterruptedException {
        // You probably need to do the same setup here you did
        // with the QFD writer
       super.setup(ctx);
        try {
            messageDigest = MessageDigest.getInstance("SHA-1");
        } catch (NoSuchAlgorithmException e) {
            // SHA-1 is required on all Java platforms, so this
            // should never occur
            throw new RuntimeException("SHA-1 algorithm not available");
        }
        // Now we are adding the salt...
        messageDigest.update(HashUtils.SALT.getBytes(StandardCharsets.UTF_8));
    }



    @Override
    public void map(LongWritable lineNo, Text line, Context ctxt)
            throws IOException, InterruptedException {

        // The value in the input for the key/value pair is a Tor IP.
        // You need to then query that IP's source QFD to get
        // all cookies from that IP,
        // query the cookie QFDs to get all associated requests
        // which are by those cookies, and store them in a torusers QFD

        //deserialize srcIP QFDs using objectinputstream
        String torIP = line.toString();
        MessageDigest md = HashUtils.cloneMessageDigest(messageDigest);
        md.update(torIP.getBytes(StandardCharsets.UTF_8));
        byte[] hash = md.digest();
        byte[] hashBytes = Arrays.copyOf(hash, HashUtils.NUM_HASH_BYTES);
        String hashString = DatatypeConverter.printHexBinary(hashBytes);
        // WTRKey key = new WTRKey("srcIP", hashString);
        Path path = new Path(String.format("qfds/%s", hashString));

        FileSystem filesys = FileSystem.get(ctxt.getConfiguration());
        FSDataInputStream fsStream = new FSDataInputStream(filesys.open(path));
        ObjectInputStream objectStream = new ObjectInputStream(fsStream);
        try {
            QueryFocusedDataSet qfds = (QueryFocusedDataSet) objectStream.readObject();
            Set<RequestReplyMatch> qfdsMatchesHash = qfds.getMatches();
            for (RequestReplyMatch match : qfdsMatchesHash) {
               
                RequestReplyMatch obj = match;
                WebTrafficRecord request = obj.getRequest();
                WebTrafficRecord reply = obj.getReply();
                String username = obj.getUserName();
                String cookie = obj.getCookie();


                //Hashing cookie
                MessageDigest cookiemd = HashUtils.cloneMessageDigest(messageDigest);
                cookiemd.update(torIP.getBytes(StandardCharsets.UTF_8));
                byte[] cookiehash = cookiemd.digest();
                byte[] cookiehashBytes = Arrays.copyOf(cookiehash, HashUtils.NUM_HASH_BYTES);
                String cookiehashString = DatatypeConverter.printHexBinary(cookiehashBytes);
                WTRKey cookiekey = new WTRKey("cookie", cookiehashString);
                Path cookiepath = new Path(String.format("qfds/cookie/cookie_%s", cookiehashString));

                FSDataInputStream fsCookieStream = new FSDataInputStream(filesys.open(cookiepath));
                ObjectInputStream objectCookieStream = new ObjectInputStream(fsCookieStream);

                try {
                    QueryFocusedDataSet cookieqfds = (QueryFocusedDataSet) objectCookieStream.readObject();
                    Set<RequestReplyMatch> cookieqfdsMatchesHash = cookieqfds.getMatches();
                    for (RequestReplyMatch cookieMatch : cookieqfdsMatchesHash) {
                        RequestReplyMatch cookieobj = cookieMatch;

                        //Hashing username
                        MessageDigest usrmd = HashUtils.cloneMessageDigest(messageDigest);
                        usrmd.update(torIP.getBytes(StandardCharsets.UTF_8));
                        byte[] usrhash = usrmd.digest();
                        byte[] usrhashBytes = Arrays.copyOf(usrhash, HashUtils.NUM_HASH_BYTES);
                        String usrhashString = DatatypeConverter.printHexBinary(usrhashBytes);
                        // WTRKey usrkey = new WTRKey("username", usrhashString);
                        // Path usrpath = new Path(String.format("qfds/cookie/cookie_%s", cookiehashString));

                        WTRKey torkey = new WTRKey("torusers", usrhashString);
                        ctxt.write(torkey, obj);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }


        




    }
}
